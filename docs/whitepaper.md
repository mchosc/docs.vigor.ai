---
id: whitepaper
title: VIGOR Whitepaper
sidebar_label: Whitepaper
---
##### *Abstract — **VIGOR** is a crypto-backed decentralized stablecoin on the EOS blockchain that tracks the US dollar.*

## I. INTRODUCTION
The VIGOR stablecoin is a financial engineering innovation with regards to a decentralized stable unit of account.  
This project creates a crypto-backed stablecoin without a
central counterparty by enabling participants to separate and
transfer both **volatility risk** and price **event risk** through
open source escrow smart contracts. Stablecoins are created
and loaned out when EOS native crypto tokens are put into
escrow as collateral and backed by insurers. This project
introduces a decentralized system of borrowing & insuring;
a crypto credit facility with only two distinct independent
participants:
+ **Borrowers**  
    – escrow EOS native tokens as collateral  
    – take stablecoin loans, maintaining collateral level  
    –  pay premiums over time to insure their collateral  
+ **Insurers**  
    – escrow EOS native tokens as insurance assets  
    – be rewarded in premiums based on contribution to solvency  
    – bailout: take-over & recap undercollateralized loans  
### A. The Problem
Currently there is no decentralized stablecoin on EOS.  
Many use cases involving time value of money require
this kind of basic functionality. Other projects suffer the
following problems that this project will attempt avoid or
improve upon:  
+ **Intractable Governance**  
    – governance token concentrated to a few whales leading to manipulation  
    – voter apathy, voters expected to vote on complex topics for which they have no interest nor specialty  
    – making bug fixes and software upgrades especially on ethereum require odd workarounds requiring central control or bloat  
+ **Weak financial engineering**  
    – arbitrary loan pricing with no financial model nor market price discovery  
    – complete lack of risk modeling  
    – participants having no way to know if risk/return is attractive  
    – no stress testing is done nor considered  
    – underestimating frictions of bailing out impaired financial products  
+ **Not scalable**  
    – arbitrarily restricting users to use only low leverage  
    – poor user experience such as transaction fees and slow block times  
### B. The Solution
VIGOR stablecoin system allows users to borrow stablecoin against their EOS crypto. It facilitates the transfer of volatility and price event risk embedded in token prices. No
mechanism yet exists on EOS mainnet for seperating and
transfering these risks. Users can do the following:  
+ **Income**
    – be rewarded income on EOS native tokens by escrowing them for use as insurance assets that back stablecoin loans  
+ **Leverage**  
    – get up to 10x leveraged exposure to token prices by
taking secured stablecoin loans followed by selling
it for EOS  
+ **Hedge**
    – token holders can reduce exposure to prices by
taking secured stablecoin loans followed by hodling
stablecoin, effectively overlaying a put option on
the crypto  

VIGOR is structured as a decentralized autonomous community (DAC) for the advantages this governance structure provides. Elected custodians manage multisig access to update
the contract code. The custodians will make critical operating
decisions and will hopefully be experts in their respective
fields. The governance token is called VIG. It’s utility is to
provide access to the system, to be used as a fee token, and
to be used as a final reserve (see subsection II-F). Custodian
elections will be facilitated by randomly selecting users
and requiring them to cast a vote when transacting on the
system. Both borrower and insurer will have a voice in the
elections. VIG tokens are paid-in by borrowers to purchase
loan insurance and a cut is held by the system to be used
as final reserve. The VIG token distribution methodology
will try to discourage whale-sized holders, by distributing
equally to all DAC genesis custodians, and by airdrop. The
bailout mechanism is low friction and does not require
auctions but rather the insurers simply take possession of
the remaining collateral and debt of failed loans thus the
illiquidity risk is offloaded to insurers who are compensated
to take this risk. VIGOR is built on EOS to take advantage
of no user transaction fees and fast blocktimes. Some unique
requirements of the VIGOR system can perhaps be met using
the new liquidapps.io DAPP network powered by DSP’s
with vRAM and forthcoming vCPU. Specifically this will
enable the VIGOR project to benefit from the ability to
generate free escrow accounts for users, store large datasets
of historical prices for risk and stress testing calculation, use
oracles for obtaining the price datasets (though at first we
are considering Delphi Oracle or Oraclize), obtain random
deviates required for risk simulation, and execute cpu intensive algorithms for pricing and stress testing. We are aware
that Block.one may build stablecoin tech into the base layer
eos.io but to date there is no evidence, and it may be a good
thing to have competing systems. Users will be able to stake
their tokens (both collateral and insurance asset tokens) even
when locked in escrow to utilize their ram and net resources
however the system will automatically begin unstaking if
collateral levels fall too low relative to user debt. VIGOR was
designed from day one to have solid financial engineering
specification using structured products and derivatives along
with standards in regulatory risk management. For example
our smart contract implementation focuses heavily on the
duality:  
+ **Pricing:** valuation model & price discovery  
+ **Risk:** risk framework, stress model, & capital adequacy  

The result is a self-sustaining ecosystem balanced by borrowers and insurers which is robust to extreme price events.
VIGOR is a crypto-backed stablecoin system with relatively tractable governance, higher leverage capability for
borrowers, and higher adoption/scalability than has ever been
possible.

## II. RISK FRAMEWORK
### A. Credit Enhancement
Two types of credit enhancement are used which enables
prudent lending:  
+ Overcollateralization (margin or haircut) refers to
holding an amount of collateral that exceeds the value of the loan and can provide a buffer against fluctuating
collateral prices.  
+ Insurance is used to protect collateral value against
catastrophic price events.  
    – Stablecoin borrowers insure their collateral by paying premium protection payments over time on
a Token Event Swap (TES) an innovative smart contract which triggers bail-out if a price event
occurs in exchange for premium payments.  
    – Insurers take on risk to be rewarded the premium and
provide loan backing by escrowing crypto tokens
into an insurance asset pool.

### B. Solvency
The stablecoin will have stable value to the extent that
loans either have excess collateral or that the insurance is
sufficiently capitalized. Therefore our smart contract models
the capitalization of the insurance with critical importance.
The system applies the Solvency II risk framework used by
insurance regulators in the EU.  
+ Solvency Ratio measures insurers ability to bail-out
undercollateralized loans, see Figure 1.  
    – Own Funds is the amount of crypto collateral insurers have escrowed (assets) above the market
value of TES insurance that borrowers have purchased (liabilities).  
    – Solvency Capital Requirement (SCR). A natural question is how much ”Own Funds” is sufficient?
SCR is defined as this required amount, for a given tolerance of certainty.  
    – Solvency Ratio > 100% is an example of a desirable limit set by VIG custodians.  

**Fig. 1. Economic Balance Sheet**
![Fig.1 Economic Balance Sheet](/img/whitepaper_fig_1.png "Fig.1 Economic Balance Sheet")

SCR is obtained as shown in Figure 2 as the change in Own Funds between normal and stressed markets. To arrive at SCR the core operation is to conduct a stress test per Solvency II which provides information about the required quantity of crypto collateral that should be escrowed by insurers to maintain solvency. For this stress test the project implements a TES pricing model to provide a best estimate for market value of the TES liabilities in normal conditions and a stress model for what their shocked value might be (given varying levels of certainty). Own Funds equals the amount of crypto collateral pledged by insurers minus our best estimate **normal** market value of the TES contracts. Stressed Own Funds equals the stressed value of crypto collateral pledged by insurers minus our best estimate **stressed** value of the TES contracts. Finally, SCR is the change in Own funds due to stressed markets and **Solvency Ratio** is the ratio of Own Funds to SCR.

**Fig. 2. Solvency Capital Requirement (SCR)**
![Fig. 2. Solvency Capital Requirement (SCR)](/img/whitepaper_fig_2.png "Fig. 2. Solvency Capital Requirement (SCR)")

### C. Stress Model
The stress model is a copula based Monte Carlo simulation
of correlated extreme price events. The simulation generates
the portfolio loss distribution as in Figure 3. Results of the
stress model has three categories of loss, expected loss which
is backed by overcollateralization, unexpected loss which
is backed by insurers, and stress loss which is backed by
VIG final reserves (see subsection II-F). Three key inputs to
this model are probability of each collateral token having an
extreme price event, their correlation, and the amount lost (1-
recovery) due to an event. Probability (derived from hazard
rate) and recovery are obtained from TES market prices, see
subsection III-B Price Discovery. Correlations will be modeled from token returns through common latent factors and
a stressed scenario of the correlation structure will be used.
This stress model is used primarily to simulate unexpected
loss to obtain SCR and Solvency Ratio but will also provide
a measure of capital efficiency and risk concentration (Risk
Adjusted Return on Capital and contribution to RAROC) to
indicate system health.  

### D. Structured Product
All TES loan insurance contracts written to insure collateral are taken together as a basket of TES to form an
aggregated insurance premium. Insurers take the other side
by selling protection on notional amounts of a basket TES (a
single TES written on a basket of collateral), see Figure 4.
The basket TES may be tranched in later releases.

**Fig. 3. Stress Model: Portfolio Loss Distribution**
![Fig. 3. Stress Model: Portfolio Loss Distribution](/img/whitepaper_fig_3.png "Fig. 3. Stress Model: Portfolio Loss Distribution")

**Fig. 4. Structured Product: transferring risk with a basket of token event swaps (TES) and a single TES written on a basket of collateral**
![Fig. 4. Structured Product: transferring risk with a basket of token event swaps (TES) and a single TES written on a basket of collateral](/img/whitepaper_fig_4.png "Fig. 4. Structured Product: transferring risk with a basket of token event swaps (TES) and a single TES written on a basket of collateral")

### E. Bail Outs
A TES is triggered for bail-out if collateral value falls
below the value of debt for a given loan. If a TES is triggered
then the basket TES protection sellers would immediately
take-over (take ownership of) and recap the undercollateralized loan realizing a loss. Gains and losses of the
insurance asset pool is shared accross sellers. Participation is
commensurate with **contribution to solvency** defined as the
change in Solvency Ratio resulting from a given TES seller
escrowing tokens into the insurance pool. Handling bail-outs
does not require auctioning collateral into distressed markets;
the basket TES is both funded and physically settled.

### F. Final Reserve
Premiums paid-in by borrowers is required to be denominated in VIG tokens and must be posted prior to drawing
loans; insufficient maintenance of VIG balance triggers bailout of the loan with borrower retaining any excess collateral.
Insurers are paid denominated in VIG.
The system stores a cut of VIG premiums as final reserves
after making VIG payment to insurers. VIG **final reserves** is
used recap the system if at any time the insurance asset pool
is depleted, covering the so-called stress losses as depicted
in Figure 3.

## III. PRICING FRAMEWORK
### A. Pricing Model
The **token event swap TES** contract delivers a protection
payment (the cost to bail-out an undercollateralized loan) at
the time of the triggering event, defined as the token price
declining below a pre-specified triggering barrier level (value
of collateral falling below the value of debt). In exchange the
TES protection buyer makes periodic premium payments at
the TES rate up to the triggering event.
The TES pricing model is based on the jump-to-default
extended constant elasticity variance model by P. Carr &
V. Linetsky [1] known as JDCEV where the token price is
modeled as a diffusion, punctuated by a possible jump to
zero:
![Math1](/img/whitepaper_math_1.png "Math1")
token volatility, and the state dependent jump-to-default
intensity (hazard rate).
To capture the negative link between volatility and token
price, we assume a constant elasticity of variance (CEV)
specification for the instantaneous token volatility prior to
extreme price events:
![Math2](/img/whitepaper_math_2.png "Math2")
where &beta; is the volatility elasticity parameter and *a* is the
volatility scale parameter.
To capture the positive link between extreme price events
and volatility, the hazard rate is an increasing affine function
of the instantaneous variance of returns on the underlying
token:
![Math3](/img/whitepaper_math_3.png "Math3")
where *b* is a constant parameter governing the stateindependent part of the jump-to-default intensity and *c* is a constant parameter governing the sensitivity of the intensity to the local volatility *&sigma;&#178;*.  
The TES price (aka premium rate) is obtained following
Mendoza-Arriaga & Linetsky [2] as the rate &rho; that equates
the present value of the protection payoff to the present value
of the premium payments.  
The protection payment is the specified percentage (1−**r**)
of the TES notional amount that the TES pays out to takeover the undercollateralized loan (**r** is the ”recovery rate” and 1 − **r** is the ”loss-given-the-triggering barrier crossing event”):
![Math4](/img/whitepaper_math_4.png "Math4")
The first term in parenthesis is the payoff triggered by a jump
and the second term is the payoff if the token price hits the
barrier by diffusion. The present value of all premium payments made by the
TES protection buyer is:
![Math5](/img/whitepaper_math_5.png "Math5")
As collateral price and volatility change over time the
premium charged to the borrowers (pricing) is adjusted using
the TES pricing model; borrowers actually pay a floating
premium rate. Premiums adjust inversely proportional to
collateralization levels and proportional to level of collateral
token volatility.
The basket TES is priced as a DV01 weighted average basket of TES spreads and the tranche pricing uses a Gaussian
copula factor model as in Wang et al. [3].

### B. Price Discovery
This section describes how market based price discovery is
achieved. TES model prices offered to borrowers are scaled
higher or lower to drive Solvency Ratio to a target set by
custodians (such as 100%). The ”right” price should lead to
a balance between loan collateral and insurance assets. The
concept is similar to option market makers updating implied
volatility based on order book imbalances. Three parameters
in the pricing and risk models are scaled in an effort to
drive Solvency Ratio closer to its target: hazard rate function
parameters, b and c as in Eq. 3 as well as recovery r as in
Eq. 4.

### C. Stability
The stablecoin is designed to be price stable to USD using
the following four pillars of stability:
1) **Crypto-overcollateralized**  
Stability depends firstly on the level of overcollateralization which covers expected losses. The system
prices the collateral in USD, and overcollateralization
is defined as USD value of collateral minus number of
stablecoins of debt. This system is extendable to create
stablecoins that track anything with a price including
other fiat currencies, baskets of fiat, low vol baskets of
crypto etc.
2) **Collateral is insured**  
Event risk and volatility of the collateral is transferred
to insurers. Stability then also depends on the level
of capital adequacy or solvency of the insurance. The
system scales insurance pricing through implied hazard
rates and recovery rates to drive Solvency Ratio to the
target set by manager vote.
3) **Final Reserve**  
The insurance asset pool represents capitalization to
cover unexpected losses estimated by the stress model
to a degree of certainty specified by VIG custodians.
Actual losses may prove worse than estimated due
to model risk. So the VIG final reserve backs the
insurance pool as a lender of last resort covering these
so-called stress losses.
4) **Solvency target**  
Custodians set the target solvency giving them the
power to run the insurance business from conservative
to aggressive.

## IV. GOVERNANCE
The VIGOR project is a decentralized autonomous community run by custodians voted in by users. Custodians vote
on all issues concerning the running of the DAC. Tools
and dapps will be developed through community worker
proposals. Initially the DAC is run by a core team called
genesis custodians as the platform is being created and
implemented. We plan to adopt the same or similar DAC
framework as eosDAC.

## V. TOKEN LENDING FACILITY
This paper has mainly addressed borrowing of stablecoin
through a crypto credit facility. Here we introduce future
capabilities for a crypto lending facility which allows borrowing and lending of crypto tokens through the use of an
upside TES and an implied zero cost collar. A lender will be
able to escrow crypto tokens for lending and be rewarded insurance
premium for taking exposure to bail out risk of upside price
events. A crypto token borrower would post stablecoin as
collateral and pay upside TES insurance premiums as they
borrow crypto tokens and take the other side of the collar.
This creates another insurance pool and structured product
for a basket of upside TES. This is distinctly different than
REX and Chintai as they only deal with resource lending,
whereas the VIGOR project deals with lending the entire
token asset so includes price volatility and price event risk
(in other words capital gains/losses). The final reserve for
this insurance pool may also serve as a source of liquidity to
facilitate new lenders entering and current lenders wanting
to exit.

## VI. OPTIONS
In a later release this project seeks to implement options
trading. This is a direct use case for our multisig escrow
contracts and stablecoin. We plan to enable long call options,
long put options, selling covered calls, and selling cash
secured put options. It will enable decentralized derivative
trading between users without central custody or clearing.

## VII. TOKEN ALLOCATION
The utility token VIG has three main utilities in the
system:
+ **fee token** stablecoin borrowers must buy loan insurance
which is paid over time and denominated in VIG tokens
+ **final reserve** for backing of stress losses
+ **access/credit score** users need VIG to access the system
and their credit score is a function of total VIG paid in
over time (and number of late payments/collections)

The VIG token will have an initial supply of 1b tokens
with 0% annual inflation rate and will be allocated as follows:
+ **20% community:** via free airdrop for wide distribution
+ **50% developer fund:** for research, engineering, deployment, business development, marketing, distribution, staking resources etc.
+ **30% DAC long term fund:** for long-term network
governance, partner support, academic grants, public
works, community building, etc.

## VIII. CONCLUSIONS
The VIGOR stablecoin system innovates the cryptobacked decentralized stable unit of account. The system
creates a decentralized credit facility that enables trustless
crypto-secured financing. It creates the first decentralized
market where borrowers and insurers interact to separate
and transfer both volatility risk and event risk embedded in
token prices. Thus the system creates the stablecoin utilizing
token event swaps TES’s and financial product structuring in
a standard regulatory risk-based capital framework. Market
based price discovery is a key feature which minimizes
price inefficiencies to the benefit of users. The system
focuses on financial engineering specification of risk, onchain stress testing, price modeling and price discovery
to ensure sufficient backing of the stablecoin and should
provide for transparent and concise voting agendas. The bailout mechanism is low friction; designed to avoid auctioning
of collateral into distressed markets. We unlock scalability
with a system that can handle more leverage e.g. users
increase loan insurance if they have low collateral levels.
The system can be viewed as necessary protocol layer for
a robust crypto-backed stablecoin system that scales, has
tractable governance, and on top of which we can deploy
an interface with automated features that users care about.
This platform has potential for users to build a crypto credit
score a natural application of identity on the blockchain.
The VIGOR DAC is a decentralized autonomous community owned and run by its members to build stablecoin
technology.
#### REFERENCES
**[1]** Carr, P., and Linetsky, V. A Jump to Default Extended CEV Model:
An Application of Bessel Processes. Finance and Stochastics 10, 3
(2006), 303330.  
**[2]** Mendoza-Arriaga, Vadim Linetsky, Pricing Equity Default Swaps
under the Jump to Default Extended CEV Model, Finance and
Stochastics, September 2011, Volume 15, Issue 3, pp 513540.  
**[3]** Wang D., Rachev S.T., Fabozzi F.J. (2009) Pricing Tranches of a CDO
and a CDS Index: Recent Advances and Future Research. In: Bol
G., Rachev S.T., Wrth R. (eds) Risk Assessment. Contributions to
Economics. Physica-Verlag HD

#### [Original Whitepaper PDF](https://vigor.ai/vigor.pdf)