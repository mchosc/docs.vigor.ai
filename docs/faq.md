---
id: faq
title: FAQ
sidebar_label: FAQ
---
## Is there an MVP?
The MVP is deployed here: **[app.vigor.ai](https://app.vigor.ai)**. We invite developers to join us in building VIGOR here: **[t.me/VIGORdev](https://t.me/VIGORdev)**

## At what point does the system issue VIGOR?
VIGOR tokens are issued by the contract when a loan is taken. This happens when the borrowers lock up tokens as collateral. The tokens can either be EOS or a portfolio of crypto tokens supported by the system.

## Can a borrower get their crypto back?
Borrowers can get their crypto back by refunding their borrowings with VIGOR which is then retired.

## What do I need in order to secure a "loan" in VIGOR?
For a borrower to receive a VIGOR loan, the borrower needs to lock in their crypto collateral and deposit the appropriate amount of VIG tokens, calculated as a percentage of VIGOR to be used as premiums to secure the loan collateral. The system requires that your collateral contains some VIG so it can take premiums every period.

## What happens when the value of my collateral falls below the amount I borrowed in VIGOR?
When your collateral drops below the value of the loan issued, the loan will enter bailout. At this point the lenders take over and recapitalize the undercollateralized loan to ensure system health.

## Am I only allowed to lock my EOS as collateral or can I also add other assets in my portfolio?
The system will allow for a portfolio to work as collateral backing a single loan.

## What additional crypto can I include in my portfolio as collateral?
Many EOS native tokens, chosen by users. In order to guarantee system health, custodians have multisig permissions to add and remove collateral types.  

## How does the VIGOR protocol work?
The VIGOR protocol is decentralized and open source. Anyone has the ability to lock EOS tokens as collateral and issue VIGOR against it.

## Is there a VIGOR testnet?
The MVP is here: [try.vigor.ai](https://try.vigor.ai)

## What is the maximum supply of VIG?
1,000,000,000 VIG tokens. VIG circulating supply will always be less than that as some of the VIG awarded to by Lenders is held in the Final Reserve.

## What are the use cases for VIGOR?
Incentive: Be rewarded by using EOS as the insurance backing VIGOR
Leveraging: use up to 10x leverage by taking secured VIGOR loans followed by selling it for EOS
Hedging: reduce exposure to price volatility by taking secured VIGOR loans followed by hodling VIGOR

## What is VIG?
The fee-utility token is called VIG. It’s utility is to provide access to the system, to be used as a fee token, and to be used as a final reserve.

## What triggers a bailout?
Premiums are required to be denominated in VIG tokens and must be posted prior to drawing loans; insufficient maintenance of VIG balance triggers bail-out of the loan with borrower retaining any excess collateral. Lenders are rewarded and incentivized with VIG.

## What is the Final Reserve?
The system automatically stores a portion of VIG insurer rewards to build up a Final Reserve. The VIG Final Reserve is used independently by the Smart Contract to rebalance the system if at any time the insurance pool is depleted. Nobody has control over the Final Reserve, only the decentralized Protocol and Smart Contract itself.

## What is the cost to use VIGOR?
The cost of the insurance adjusts based on how risky the system is relative to a target; this is market price discovery without the need for trading or an order book. The risk model considers levels of overcollateralization, debt and insurance assets. It is fully on-chain and mimicks standards for classical insurance; we call this on-chain risk and compliance.

## Why the need for overcollaterization and a Final Reserve?
There are three levels of volatility protection for low volatility payment token loans. Borrowers over collateralized their loans protecting against normal volatility. Lenders post tokens as insurance assets which provide further protection against price jump risk. The final reserve provides a third layer of volatility protection in case the insurance pool depletes, and is more formally described as the buffer that covers stress losses or model risk.

## How is my collateral secured?
VIGOR appoints 21 temporary Custodians on a daily basis, chosen among the fully free and decentralized autonomous community, who control for safety and security purposes multi-sig permissions to 3rd party audited smart contracts.

## How does the VIGOR price defeat volatility?
VIGOR should be valued externally with a low volatility quotation, because users have incentive to stake enough crypto collateral so that even during stressed volatility periods there would be at least a minimum amount of collateral to protect each VIGOR’s value. The contract continuously stress tests the system and autonomously updates the insurance premiums. If solvency is below target premiums rise to attract more insurance collateral and slow further borrowing. When users make/take loans it is a credit/debit to the system risk budget. With confidence that the system has sufficient collateral so that it is adequately capitalized to survive high volatility conditions, traders can soft arbitrage by taking loans or unwinding loans to their advantage if the value of VIGOR departs from its baseline.

## How are liquidation penalties distributed?
VIGOR doesn’t charge liquidation penalties. When your loan is liquidated due to a low collateral ratio, many other protocols will also hit you with a liquidation penalty. Some of these are as high as 20%. VIGOR doesn’t charge ANY additional fees that are commonly seen on other protocols (ie administration fees, stability fees etc).

## Why build VIGOR with other low-volatility tokens on the market?
VIGOR is designed from the ground up using time tested financial modeling. The VIGOR protocol actively evaluates system level risk and continually adjusts to reduce that risk. Other tokens use simple overcollateralization techniques to try to limit risk but do not actively evaluate and manage it. These other systems, in general, rely on secondary auction markets to sell off bad debt in case of a Black Swan event. VIGOR’s loan debt is automatically transferred to the insurance pool (then the final reserve) without relying on secondary markets to sell the bad debt to regain solvency.

## How does VIGOR compare to other platforms?
[See the Comparison Table here.](https://vigor.ai/#compareDefi)

## Why use the EOS blockchain?
There are many reasons why VIGOR has decided to build on EOSIO technology. Without going into great detail we can sum it up by mentioning a few features that EOSIO offers. These include:
Speed: 0.5 Second block times ensure quick access to funds when necessary. This is a critical component for every decentralized platform.
Security: EOSIO is built with advanced account permission capabilities. There are endless combinations of multisig and custom permissions available on EOSIO that enable the full range of user needs while providing unprecedented security.
Battle tested code base- C++ smart contracts: Using a C++ code base allows developers to reference previously tested code from a plethora of code repositories.
Feeless: Resources are renewable and projects have the ability to cover user resources to allow their users to transact for free on their platform.
Ability to upgrade operating code: Continued improvements/critical updates can be made to code with little to no impact on operating platforms.
Interoperability: EOSIO is built with chain interoperability in mind. Many EOSIO chains already exist and there are secondary solutions (Dapp Network) that provide the opportunity to access the VIGOR decnetralized platform for more users, across multiple blockchains.

## Why do I need to create VIGOR if I can buy it on the secondary market?
Taking out a VIGOR loan allows you to maintain exposure to the crypto market (ie maintain ownership of your EOS) while providing you with some liquidity. You can leverage your crypto holdings by taking out a VIGOR loan and purchasing additional crypto with it. Additionally, you can take out a crypto loan (i.e. posting VIGOR to get EOS) and sell it on secondary markets to be able to go short.

## Is the smart contract code publicly available?
Yes, please visit our **[repository](https://gitlab.com/vigorstablecoin)**

## Who’s on the team?
There is no predetermined team, controller or owner of the VIGOR Protocol. VIGOR on a daily basis appoints 21 temporary Custodians, chosen among the fully free and decentralized autonomous community. Custodians are responsible for building the VIGOR protocol according to the whitepaper and securing the multisig.
To view the top 21 at any given time please see the Vigor Custodian Board.

**[Custodian Board](https://vigor.ai/community#custodianBoard)**

## What's the main differentiators between VIGOR and MakerDAO?
VIGOR improves on what we think are shortcomings to legacy MakerDAO:  
1. On Makerdaos’s system borrowers are slaves with no voice ruled by MKR whales, and
the role of lenders/governor is entangled into one, same as traditional lenders. On
VIGOR we have two user types both with voting rights. Borrowers pay a premium to take
low volatility payment token loans against collateral and another type of user, lenders, also post collateral
to back those loans to be rewarded the premium. Both of our user groups have voting rights and
delegate their desires to custodians. Like the idea of bank run by both borrower and
lender? That’s the VIGOR model.
2. VIGOR will extend into letting users borrow EOS tokens against low volatility payment token collateral
(for short selling). Makerdao system cannot do that.
3. Makerdao does not measure risk, and pricing is arbitrarily chosen which leads them to
set overly conservative leverage limits. This limits scale since their only user base is low
leverage hodlers. We recognize that low volatility payment tokens are about price jump risk (default risk)
and the need to transfer/insure against jumps. Platforms that ignore jump to default risk
are playing a dangerous game. We designed our smart contracts with clearly defined onchain risk and pricing based on equity default swaps and Solvency II risk based capital
requirements. Our market determined price discovery unlocks the ability to set higher
more efficient leverage limits and onboarding short term traders who want higher
leverage.
4. MKR governance is intractable. Voter participation is near zero (expect for whales who
dominate and push their agendas through easily). MKR voters are supposed to vote on
risk and pricing, which is laughable because MKR holders are not necessarily skilled in
those areas, and agreeing on complex model/price is impossible. VIGOR governance is
more tractable because risk and pricing is built-in (on-chain) and voters simply delegate
their interests to elected DAC custodians who are experts or can hire experts.
5. Makerdao bailouts are high friction. They must auction collateral and MKR into
distressed markets, precisely when there will be no buyers. VIGOR bailout mechanism is
low friction. Backers post collateral ahead of time, and is ready to recap loans. Also with
maker it is unclear if there is a reserve available during a black swan (rumor is that
founders will pony up to save the day), we explicitly have a reserve that backs the
backers.
6. Makerdao MKR holders have no idea what their risk/return profile looks like. The
VIGOR system is run like an insurance business with a solvency measure used by
regulators in the insurance world (Solvency II), the risk and pricing is explicitly specified
in the whitepaper (equity default swaps) and calculated on-chain for unprecedented
transparency. VIGOR may tranche the lenders into junior (takes first loss) and senior for
better user experience. Performance measures are also on chain such as RAROC (risk
adjusted return on capital)
7. Our platform is multicollateral, but Makerdao is fake-multicollateral. Makerdao is really
just allowing to have separate single-collateral backed loans, for each loan you can
choose a different collateral type, they didn't design for portfolio risk). VIGOR considers
the user total portfolio of collateral backing a single loan.

## Is the VIGOR platform like Bitshares/BitUSD?
bitUSD is based on having an exchange, with an order book for traders of exotic CFD's
(contracts for difference), each side putting up collateral and placing bid or ask. The exotic
feature enables the long side (bitUSD holder) to be fungible by defining a set of heuristics
where the long is allowed to settle their position at any time with whoever happens to be
the least collateralized at that time at a trusted settlement price, along with some margin
call features)
Bitshares uses a limit order book for price discovery and requires trading. While that
provides efficient pricing we think this is too cumbersome and will dampen scale. Also
note that Maker platform has no price discovery, occasionally the loan rate is arbitrarily
reset by voters (basically a few whales). This is an example of inefficient pricing which
leads to a need to apply overly conservative leverage limits and high penalties for
liquidation, again limiting scale. Our platform is in between these two extremes with
respect to price discovery and scale.
Our VIGOR low volatility payment token platform has no order book or trading (but still has a market
determined price discovery mechanism). The platform is something like borrowing cash
against a house and paying mortgage insurance (pmi) with simple advertised adjustable
rates for borrowers to pay on their individual loans and lenders to be rewarded from the pool of
loans. It centers on a credit default swap variant (equity default swap), a structured
product, and an insurance industry risk-based capital model where insured loan rates
adjust to drive solvency of the system to a target. If a user wants a low volatility payment token loan, they
post collateral tokens and are required to insure them by paying a periodic premium to
buy the protection leg of an equity default swap (eds). Premiums from a basket of these
eds flow to the lenders who are sellers of a single eds written on that same basket of
collateral, which is funded meaning that lenders have escrowed tokens ahead of time to
be available to cover bailouts, and physically settled meaning lenders take possession
of the impaired collateral and debt in the event of default (loan becomes
undercollateralized), recapping the loan. The lenders are further backed by a reserve
that grows over time as it absorbs a cut of all premiums.

