---
id: usr-wit-ins
title: Withdraw Insurance
sidebar_label: Withdraw Insurance
---
Before make a withdrawal, please ensure you have paid past due amount by sending VIG into collateral.

## HOW TO WITHDRAW INSURANCE VIA DEMO INTERFACE
* Login to the Vigor App at:
```
https://try.vigor.ai
```
* You must have insurance deposited **([USER INSURANCE HOW TO](./usr-insur))** in order to withdraw from insurance.
* Find the **Insurance** box
* The box shows the amount of insurance tokens already present (if any) and their value 
* Select the token from **drop down menu** 
* Fill the field with the desired amount
* Click on **WITHDRAW**
* Confirm Scatter pop-up

## HOW TO WITHDRAW INSURANCE VIA BLOKS.IO
* Go to: **bloks.io &#x2192; vigorlending &#x2192; Contract &#x2192; Actions &#x2192; assetout**
* usern: *YOURUSERNAME*
* assetout: *THE AMOUNT OF TOKEN YOU WANT TO WITHDRAW*
* Memo: **insurance**
* Click on **Submit Transaction**
* Confirm Scatter pop-up

## Please note
* Your insurance assets are shown in the **"Insurance"** column of the contract

The columns representing the variables in the contract table can be found here:

**bloks.io &#x2192; vigorlending &#x2192; Contract &#x2192; Tables &#x2192; User**

Vigor Crypto Lending Protocol:
```
https://vigor.ai
```