---
id: usr-get-start
title: Vigor Jungle Testnet Tutorial
sidebar_label: Getting Started
---

## WHAT YOU NEED FOR THE TEST
* An **account on Jungle Testnet** to login into the demo interface or contract on bloks.io
* A few **Jungle EOS** 
* (optional) you can **setup Scatter** for a faster and easier login


## HOW TO CREATE AN ACCOUNT ON TESTNET
*  Go to:
```
http://monitor3.jungletestnet.io
```
*  Click on **Create Key**
    * copy/save you pair 
*  Click on **Create Account**
    * Choose an **Account name** with 12 characters (a-z,1-5)
    *  Paste your keys
    *  Verify **reCaptcha**
    *  **Create**

## HOW TO GET EOS ON JUNGLE 
*  Go to:
```
http://monitor3.jungletestnet.io
```
* Click on **Faucet**
* Write your **Account name**
* Verify **reCaptcha**
* Click **Send Coins** (100 EOS will be credited to your account)
* (don’t be scared by the red text. Everything is OK.)
* Come back after 6 hours to claim more free Jungle EOS
* You will also find a VIG **Faucet** button when you log into **[try.vigor.ai](https://try.vigor.ai)** in the sidebar menu.

## HOW TO SET UP SCATTER (optional)
Download **Scatter** from
> Latest
>>```
>> https://get-scatter.com/download 
>> ```
> Stable Version (11.0.1)
>>```
>>https://github.com/GetScatter/ScatterDesktop/releases/tag/11.0.1
>>```
* Open **Scatter**
* Go to **Administrative &#x2192; Networks &#x2192; Add custom network**
    * Find Network Api list : [http://monitor3.jungletestnet.io/#apiendpoints](http://monitor3.jungletestnet.io/#apiendpoints)
    * In **Network** tab
    * Name: put a name of your choice, like "**Jungle3 Testnet**”
    * Host:
    ```
    jungle3.cryptolions.io
    ```
    * Protocol: **https**
    * Port: **443**
    * Chain ID:
    ```
    2a02a0053e5a8cf73a56ba0fda11e4d92e0238a4a2aa74fccf46d5a910746840
    ```
    * **System Token** tab
    * Switch **ON** the button
    * Contract: **eosio.token**
    * Symbol: **EOS**
    * Decimals: **4**
    * **Save new network**
* Go to **Wallet &#x2192; Import Key**
* Select **Text** and **paste** your **private key**
* Back to Wallet
* Find your new network name and click on the drop-down menu near the key icon.
* While the drop-down menu is open, **press CTRL** to reveal the hidden function.
* Select **Link Account**
* Type your **account name** and check that Jungle Teestnet is correctly selected
* Press **Link Account**
* Scatter configuration is done.



Vigor Protocol Website: **[Vigor.ai](https://vigor.ai)**

Vigor App on Testnet: **[Try.Vigor.ai](https://try.vigor.ai)**
