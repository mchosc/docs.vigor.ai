---
id: release-notes
title: Release Notes
---


Currently testing on try.vigor.ai:
* **vigorlending** - core logic
* **vigortoken11** - VIGOR token contract
* **oracleoracl2** - mock delphi oracle on jungle
* **vigoraclehub** - oracle hub, fed by custodians/approved feeders (and delphipoller which queries into oracleoracl2)

Older versions:
*  **vigorlend113**
*  **vigordemo123**
*  **vigordemo111**
*  **vigor1111114**
*  **vigor1111113**
*  **vigor1111112**
*  **vigor1111111**