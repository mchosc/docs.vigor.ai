---
id: contracts
title: Protocol Accounts / Contracts
sidebar_label: Contracts
---
## VIG token contract
```
https://bloks.io/account/vig111111111
```
## VIGOR token contract
```
https://bloks.io/account/vigortoken11
```
## Vigor Protocol
```
https://bloks.io/account/vigorlending
```
Explained in **[Protocol Contract](/docs/en/mainnet/vigor_protocol)**

## Vig DAC token Agreed Constitution			
```
https://bloks.io/account/dactoken1111
```
## Custodian Actions 
claim pay, votecust, update required pay
```
https://bloks.io/account/daccustodia1
```

## Tokenholder rewards Custodian/Candidate
```
https://bloks.io/account/dacholding11
```					

## Auth account
```
https://bloks.io/account/dacauth11111
```					

## Proposal
```
https://bloks.io/account/dacmultisig1
```					

## DAC Fund
For operating costs - a small portion of loan fees goes to that account.
```
https://bloks.io/account/vigordacfund
```

## Proxy for the Community
```
https://bloks.io/account/supportvigor
```		

## Proxy for Vigor contract
```
https://bloks.io/account/vigorrewards
```			

## Custodian/Candidate donation for Dev
```
https://bloks.io/account/vigordevfund
```					

## Custodian/Candidate donation for Marketing
```
https://bloks.io/account/vigmarketing
```

## Custodian/Candidate donation for Legal
```
https://bloks.io/account/viglegalfees
```

## Vigor Oracle
```
https://bloks.io/account/vigoraclehub
```					

## Delphi Oracle
```
https://bloks.io/account/delphipoller
```					

## Final Reserve
3rd line of defense. Account where final reserve cut goes to.
```
https://bloks.io/account/finalreserve
```		

## vig.ai
```
https://bloks.io/account/vig.ai
```

## vigor.ai
```
https://bloks.io/account/vigor.ai
```	

## vigcommunity
```
https://bloks.io/account/vigcommunity
```		