---
id: media
title: Media
sidebar_label: Media
---
#### Here you find all the latest VIGOR Brand assets.
## Global Repo
> [Full Data HERE](https://gitlab.com/vigorstablecoin/media)
## VIGOR Logos
![LOGOS](/img/media/vigor-logos.jpg)
> + [EPS FILES](https://github.com/vigorstablecoin/media/tree/master/__Brand/Vigor__logos/EPS)  
> + [PNG  FILES](https://github.com/vigorstablecoin/media/tree/master/__Brand/Vigor__logos/PNG)  
> + [SVG  FILES](https://github.com/vigorstablecoin/media/tree/master/__Brand/Vigor__logos/SVG)  



## Color Palette
![Pallet](/img/media/color1.png)
![Pallet](/img/media/color2.png)
![Pallet](/img/media/color3.png)

## Vigor Typography
![typo](/img/media/typo.jpg)
> [Download Montserrat from HERE](https://gitlab.com/vigorstablecoin/media/-/blob/master/__Brand/fonts/Montserrat.zip)