---
id: scenarios
title: Why Vigor???
sidebar_label: Why Vigor???
---
![Vigor Community](/img/Vigor_Community.png)
## Scenario 1 - Borrowing & volatility
Borrowing is usually less risky than giving up your portfolio.  
Crypto volatility is one of its most peculiar elements. Cryptos are decentralized, have no central authority, no controlled outflow mechanism, and they are highly volatile. You may have 300 EOS in your portfolio worth $750, but in less than a month it could be worth $1000. How do you make sure you have funds when you need them and not miss an opportunity to possibly make a 167% profit?

One option is to expand your options by borrowing and not disrupt your carefully planned portfolio.

In reverse, you may have 300 EOS in your portfolio worth $1000, but as prices change a month later it could be worth $750. Borrowing a low volatility payment token like VIGOR can help provide a hedge against stressed market conditions and help to bridge a liquidity gap. You can recover your crypto back by repaying your loan and not disrupt your carefully planned portfolio.

***How does the VIGOR price defeat volatility? VIGOR is transparently backed by a pool of VIG tokens, and incentivizes users to deposit enough crypto collateral, so that even during stressed markets the price defeats volatility. Vigor has three levels of backing: Overcollateralization, Final Reserve, and Lenders who post tokens as insurance assets which provide further backing against price jump risk.***

## Scenario 2 - Borrow to invest 
This approach makes sense when you think about both the short and long term. You may hold a crypto that offers the potential for strong returns long-term, but at same time see a good investment opportunity for the short-term. By borrowing to invest in the short term opportunity, against your long term portfolio assets can still partake in short term opportunities that come your way without giving up your long term positions.  Simply claim your profit and repay your loan to recover your crypto back at a later period.

***How is my collateral secured? VIGOR has 21 custodians, voted on by the community, which control multi-sig permissions to 3rd party-audited smart contracts.***

## Scenario 3 - Borrow to increase diversification
Perhaps your portfolio is concentrated on a single asset. Borrowing can help fund a diversified portfolio with your crypto collateral (i.e. your crypto holdings) still intact. Such strategy involves risk and should only be undertaken by experienced traders and investors, but if properly managed, there is a potential to exceed the cost of borrowing. The VIGOR protocol is decentralized and open source. Unlike many services in the financial mainstream, anyone has the ability to Lock up collateral to borrow VIGOR tokens or other cryptocurrencies.

Am I only allowed to lock my EOS as collateral or can I also add other assets in my portfolio? The system will allow for a portfolio to work as collateral backing a single loan.

## Scenario 4 - Short selling
Short selling is an investment or trading strategy that speculates on the decline in a stock or other securities price. It is an advanced strategy that should only be undertaken by experienced traders and investors. Vigor Protocol can help you borrow (EOS, VIG or VIGOR) to short sell on some other exchange.

## Scenario 5 - Borrow to secure liquidity
You could borrow to access cash without selling your crypto holdings. You may need funds temporarily or have some other good reasons you are considering selling some of your assets. In cases such as this, the Vigor protocol can help you secure a loan in a decentralized, secured and automated way against your assets without the need to sell them.

What do I need in order to secure a "loan" in VIGOR? For a borrower to receive a VIGOR loan, the borrower needs to lock in their crypto collateral and deposit the appropriate amount of VIG tokens, calculated as a percentage of VIGOR to be used as premiums to insure the loan collateral. The system requires that your collateral contains some VIG so it can take premiums during every period.

***Why do I need to create VIGOR if I can buy it on the secondary market? Taking out a VIGOR loan allows you to maintain exposure to the crypto market (ie maintain ownership of your EOS) while at the same time providing you with some liquidity. You can leverage your crypto holdings by taking out a VIGOR loan and purchasing additional crypto with it. Additionally, you can take out a crypto loan (i.e. posting VIGOR to get EOS) and place an order on secondary markets to be able to go short.***

## Scenario 6 - Staking in Vigor
Vigor does not have a staking feature, but:
+ you can lend your VIG and have rewards from that
+ you can lock your VIGOR in savings pool and get your rewards from that