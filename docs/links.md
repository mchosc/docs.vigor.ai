---
id: links
title: Links
sidebar_label: Links
---
> ## Buy/Sell
> + [Newdex VIG](https://newdex.io/trade/vig111111111-vig-eos)
> + [Newdex VIGOR](https://newdex.io/trade/eosio.token-eos-vigor)
> + [XNation VIG](https://xnation.io/eos?base=EOS&quote=VIG)
> + [XNation VIGOR](https://xnation.io/usds?base=USDT&quote=VIGOR)
> + [Alcor VIGOR](https://alcor.exchange/markets/VIGOR-vigortoken11)
> + [Alcor VIG](https://alcor.exchange/markets/VIG-vig111111111)
> + [SX (no UI yet)](https://bloks.io/account/vigor.sx)
> + [Whale EX VIG](https://www.whaleex.com/trade/VIG_EOS)
> + [DefiBox Swap](https://dapp.defibox.io/swap)

## Telegram
+ By Language 
  - [English](https://t.me/vigorprotocol)
  - [French](https://t.me/Vigorfrancophone)
  - [Indonesian](https://t.me/vigor_indonesia)
  - [Spanish](https://t.me/vigorespanol)
  - [Turkish](https://t.me/vigorturkey)
  - [German](https://t.me/vigordeutsch)
  - [Italian](https://t.me/Vigoritalia)
  - [Korean](https://t.me/vigor_kr)
  - [Malaysian](https://t.me/Vigormalaysia)
  - [Filipino](https://t.me/vigorph)
+ [African Channel](https://t.me/VigorAfrica)
+ [Governance](https://t.me/VIGORgov)
+ [Developement](https://t.me/VIGORdev)
+ [Support](https://t.me/VIGORSUPPORT)
+ [Media](https://t.me/VIGORmedia)
+ [Marketing](https://t.me/vigormarketingandintegrations)
+ [Community](https://t.me/Vigorcommunity)
+ [International Collaboration](https://t.me/VIGORInternationalCollaboration)
+ [Product](https://t.me/vigorproduct)
+ [Social Media Posts](https://t.me/vigortwitter)
+ [Memes](https://t.me/VigMemes)
+ [Boid.com Mining Team](https://t.me/vigorboidmining)
+ [Vigor on Worbli](https://t.me/VIGORworbli)
+ [Quiz Team](https://t.me/vigorquiz)
+ [Vigor DAC BOT](@VigorDACbot)
+ [News Channel](https://t.me/vigornews)

## DAC
+ [dac.vigor.ai](https://dac.vigor.ai/)
+ [vig.ai](https://vig.ai/)

## APP & Other
+ [App Mainnet](https://app.vigor.ai/)
+ [App Try Testnet](https://try.vigor.ai/)
+ [Statistics](https://stats.vigor.ai/d/aHTjcofWz/vigor-home?orgId=1)
+ [Vigor Learn & Rewards](https://rewards.vigor.ai/)
+ [GitLab Repo](https://gitlab.com/vigorstablecoin)
+ [Directory](https://vigor.directory/)
> + [User Stats](https://stats.vigor.ai/d/HjtKTIyZz/user-quick-vigor-overview?orgId=1&from=now-7d&to=now&var-account=vigordacfund)

## VIGOR Key Documentation
+ [Summary](https://vigor.ai/VIGOR_summary.pdf)
+ Whitepaper 
  + [English](https://vigor.ai/vigor.pdf)
  + [Chinese](https://vigor.ai/VIGOR_Chinese.pdf)
  + [Spanish](https://vigor.ai/VIGOR_Spanish.pdf)
  + [French](https://vigor.ai/VIGOR_French.pdf)
  + [Italian](https://vigor.ai/VIGOR_Italian.pdf)
  + [Turkish](https://vigor.ai/VIGOR_Turkish.pdf)

## Social
+ [Twitter](https://twitter.com/vigorprotocol)
+ [Reddit](https://www.reddit.com/r/vigorstablecoin)
+ [Discussions.app](https://discussions.app/tag/vigor)
+ [Uptrennd](https://www.uptrennd.com/vigor)
+ [GenPool.io Vigor Proxies](https://genpool.io/)
  - [Vigor Support Proxy](https://www.alohaeos.com/vote/proxy/supportvigor)
  - [Vigor Protocol Proxy](https://www.alohaeos.com/vote/proxy/vigorrewards)
+ [DappReview about Vigor](https://dapp.review/dapp/12208/VIGOR)
+ [Everipedia](https://everipedia.org/wiki/lang_en/vigor-stablecoin)
+ [Facebook Group](https://www.facebook.com/groups/VigorDAC/?hc_location=group#_=_)
+ [YouTube](https://www.youtube.com/channel/UC2SXCn9e6p3HLC-UPe4dJww)
+ [Instagram](https://www.instagram.com/vigorprotocol/)
+ [TikTok](https://www.tiktok.com/@vigordac?language=en&sec_uid=MS4wLjABAAAADZtN2jWGyfRPkIkWKrpcI1YKxaRu2l_TfxmOsNfJKEQnggk7Ju7LARopCAD7YZQM&u_code=db6914jhj41em4&timestamp=1583782971&utm_source=copy&utm_campaign=client_share&utm_medium=android&share_app_name=musically&share_iid=6800787158783248133&source=h5_m)
+ [Medium](https://medium.com/@vigordac)
+ [DappRadar](https://dappradar.com/eos/1167/vigor)