---
id: dummies
title: VIGOR for Dummies
sidebar_label: VIGOR for Dummies
---
![Dummies3](/img/dummies/dummies_3.png)
## What is Vigor?
Vigor is a **P2P** (Peer To Peer) **decentralized automated Lending platform** that **allows you** to **Borrow**, **Lend**, and **Save**. It is **powered by the EOS Blockchain.** 

The **Vigor Protocol employs a two-token system**, the **fee-utility token VIG** used for fees, rewards, & collateral, and the **low volatility payment token VIGOR** backed by a pool of EOS & VIG tokens.  

### Low Volatility Payment Token
**VIGOR:** VIGOR is a **crypto-backed low volatility payment token** that utilizes the EOS blockchain. VIGOR is transparently backed by a pool of VIG tokens, and incentivizes users to deposit enough crypto collateral, so that even during stressed markets the price defeats volatility.

**Max Supply:** 100,000,000,000 **VIGOR**.  
*[View full stats HERE](https://bloks.io/tokens/VIGOR-eos-vigortoken11)* and even more stats here: **[stats.vigor.ai](https://stats.vigor.ai)**

### Utility Token
**VIG:** VIG is a **utility token** used for **fees**, **rewards**, and **collateral**. It is the **governance and fee-utility token**. Its utility is to provide access to the system, to be used as a fee token, and to be used as a final reserve.

**VIG Utility:**
+ **Reward:** Lenders and Savers receive VIG premiums.
+ **Reserve:** Pool of VIG that supports the protocol during market shocks.
+ **Reputation:** Borrowers with a good reputation get discounted rates.

**Max Supply:** 1,000,000,000 **VIG**  
*[View full stats HERE](https://bloks.io/tokens/VIG-eos-vig111111111)* and even more stats here: **[stats.vigor.ai](https://stats.vigor.ai)**

## The Vigor Platform
![App1](/img/app/app_1.png)
**Borrow: Lock up collateral to borrow VIGOR tokens or other cryptocurrencies,** the borrower needs to **lock in their crypto collateral** and **deposit the appropriate amount of VIG tokens.**
+ **Low Volatility Loan:** Deposit crypto as collateral for taking out a VIGOR token loan. Pay loan premiums in VIG token. 
+ **Crypto Loan:** Deposit VIGOR token as collateral for taking out a crypto loan. Pay loan premiums in VIG token.

**Lend & Save:** Get rewarded VIG tokens by locking your crypto in an insurance pool to back loans or by locking up Vigor in a crypto savings account, it is an automated and secured process.

The vigor platform is **decentralized, transparent** and **automated;** all activities are **on chain** and **displayed on users dashboard.**
![App7](/img/app/app_7.png)

### Staking in Vigor
Vigor does not have a staking feature, you can lend your **VIG, VIGOR** or **EOS** token to get rewards.

**How it works**
![App8](/img/app/app_8.png)

### Lend & Savings
**Become a Lender:** Lenders are rewarded in VIG tokens for depositing Cryptos and VIGOR tokens into the insurance pool to insure the system against both upside and downside market stress events.

**Lend & Savings:** Lock VIGOR tokens and get rewarded a savings rate on your funds.  
+ Lend it to others to borrow it
+ Make it available as backing for other loans

![App1](/img/app/app_1.png)

## Get familiar with Vigor
**At what point does the system issue VIGOR?** VIGOR tokens are issued by the contract **when a loan is taken.** This happens when the borrowers lock up tokens as collateral. The tokens can either be EOS or a portfolio of crypto tokens supported by the system.

**Can a borrower get their crypto back?** Borrowers can get their crypto back **by refunding their borrowings with VIGOR** which is then retired.

**What do I need in order to secure a "loan" in VIGOR?** For a borrower to receive a VIGOR loan, the **borrower needs to lock in their crypto collateral and deposit the appropriate amount of VIG tokens,** calculated as a percentage of VIGOR to be used as premiums to insure the loan collateral. The system requires that. 

**What happens when the value of my collateral falls below the amount I borrowed in VIGOR?** When your collateral drops below the value of the loan issued, **the loan will enter bailout.** At this point the lenders take over and recapitalize the undercollateralized loan to ensure system health.

**[Bailout:](https://www.youtube.com/watch?v=xLqnMWFwItc&feature=youtu.be)** Premiums are required to be denominated in VIG tokens and must be posted prior to drawing loans; **insufficient maintenance of VIG balance triggers bail-out of the loan** with the borrower retaining any excess collateral. Lenders are rewarded and incentivized with VIG.

**How to avoid loan Bailout:** To avoid loan bailout, **keep adequate collateral and VIG tokens at all times.**  
**Final Reserve: A cut of the VIG awarded to lenders are stored by the system as final reserves.** VIG final reserves are used to rebalance the system if at any time the insurance pool is depleted.

**Why the need for overcollaterization and a Final Reserve?** There are **three levels of backing for Low Volatility Loans.** Borrowers over collateralized their loans protecting against normal volatility. Lenders post tokens as insurance assets which provide further backing against price jump risk. The final reserve provides a third layer of backing in case the insurance pool depletes, and is more formally described as the buffer that covers stress losses or model risk.

**Vigor Protocol:** The smart contracts that power Vigor.

**How does the VIGOR protocol work?** The VIGOR protocol is **decentralized** and **open source, anyone has the ability to lock VIG, VIGOR or EOS tokens as collateral** and issue VIGOR against it.

**VIGOR TG Working Groups:** Visit **[https://vigor.ai/](https://vigor.ai/)** for details.

**How does the VIGOR price defeat volatility?** **Users** are **incentivized to deposit enough crypto collateral** so that even during stressed markets the price defeats volatility. The **contract continuously stress tests the system** and **autonomously updates the insurance premiums.** If solvency is below target, premiums rise to attract more insurance collateral and slow further borrowing. 

**How are liquidation penalties distributed? VIGOR doesn’t charge liquidation penalties.** When your loan is liquidated due to a low collateral ratio, many other protocols will also hit you with a liquidation penalty. Some of these are as high as 20%. VIGOR doesn’t charge ANY additional fees that are commonly seen on other protocols (ie administration fees, stability fees etc).

**How is my collateral secured? VIGOR has 21 custodians,** voted on by the community, which **control multi-sig permissions to 3rd party-audited smart contracts.**

**How does VIGOR compare to other platforms?** See the Comparison Table here: **[https://vigor.ai/#compareDefi](https://vigor.ai/#compareDefi)**

## Vigor Reputation System: 
Reputation is built by:
+ Having more debt than others over time
+ Having more in the lending/insurance/savings than others over time  

**Vigor Miners:** vigorlending ([Vigor Protocol Account/Contract](https://docs.vigor.ai/docs/mainnet/contracts)) is powered by miners, every second of the day miners execute transactions to get the VIG.  
**Vigor Stats Dashboard:** Data is monitored and collected from the vigor platform, and displayed on Vigor stats dashboard. **Get full stats: [http://stats.vigor.ai/](http://stats.vigor.ai/)**

![Stats1](/img/stats_1.png)

## The Vigor DAC (Decentralized Autonomous Community)

### Vigor DAC
Vigor is a DAC ([Decentralized Autonomous Community](https://www.youtube.com/watch?v=d7l9dbiPZxw)) **of 21 Custodians** and **150+ Candidates.** The 21 Custodians are elected from the Candidate list based on the number of votes.

### Vigor Candidate
Vigor Candidates **help to build Vigor** and **add value to the Vigor platform.** All candidates help out with best efforts, and receive votes based on input. **Candidates get daily pay in the tokens.**

### Vigor Custodian
Higher voted candidates are called **custodians,** currently there are 21 custodians and they are elected daily and are responsible for completing tasks ranging from building out the core Vigor Platform features, to marketing, legal solutions, and multisig control of the entire system.

![Dummies2](/img/dummies/dummies_2.png)

### Voting
Currently candidates can vote for up to 21 custodian candidates at a time. They vote who they think will bring value to the DAC.

### Registering as a candidate
Registering as a candidate in the DAC is open to the community, **being a candidate comes with responsibilities as outlined in the vigor constitution,** before registering as a candidate, it is advised that you understand and can meet the responsibilities of being a candidate.

**New Vigor DAC Candidates** are always welcome, **being a candidate/custodian means** you wish to help Vigor build and want to add value to the Vigor platform. Going above being a user of the Vigor platform.

**If you want to be a candidate,** you need to fill out a profile on **[http://dac.vigor.ai/](http://dac.vigor.ai/) highlight your skillset, upload a pic,** and **ask a custodian to submit a proposal for you.** If you are just a member you simply sign the constitution.

Upload a Profile PIC using this link: **[https://imgbb.com/](https://imgbb.com/)**   
You can join the DAC on: **[http://dac.vigor.ai/](http://dac.vigor.ai/)**

## What is EOS?
EOS blockchain is built on an EOSIO platform - The Most Powerful Infrastructure for Decentralized Applications

Some of the groundbreaking features of EOSIO include:
+ Free Rate Limited Transactions
+ Low Latency Block confirmation (0.5 seconds)
+ Low-overhead Byzantine Fault Tolerant Finality
+ Designed for optional high-overhead, low-latency BFT finality
+ Smart contract platform powered by WebAssembly
+ Designed for Sparse Header Light Client Validation
+ Scheduled Recurring Transactions
+ Time Delay Security
+ Hierarchical Role Based Permissions
+ Support for Biometric Hardware Secured Keys (e.g. Apple Secure Enclave)
+ Designed for Parallel Execution of Context Free Validation Logic
+ Designed for Inter Blockchain Communication

> Take a look at this great explanation on **[Blockgeeks](https://blockgeeks.com/guides/eos-blockchain/)**  
>> EOSX wrote a great guide about most important things on EOS **[HERE](https://www.eosx.io/guides/welcome).**

## EOS Name Service
EOS Name Service allows you to purchase custom and premium EOS account names using many different payment methods.
<iframe width="100%" height="500" src="https://eosnameservice.io/?ref=boidaccounts&widget=true" style="border:1px solid lightgrey;"></iframe>