---
id: vigor_app
title: VIGOR APP
sidebar_label: VIGOR APP
---
### 100% decentralized peer to peer lending brings borrowing and lending crypto to the EOS mainnet.

> The app is located here: **[app.vigor.ai](https://app.vigor.ai)**
> ```
> https://app.vigor.ai
> ```
>> To test it out with some testnet tokens go here: **[try.vigor.ai](https://try.vigor.ai)**
>> ```
>> https://try.vigor.ai
>> ```
The first step is to login to the app using your crypto wallet. Scatter or Anchor are supported at the moment. 
> You can get them from here:  
> **[Scatter](https://get-scatter.com/)**
> ```
> https://get-scatter.com/
> ``` 
> or **[Anchor](https://greymass.com/en/anchor/)**
> ```
> https://greymass.com/en/anchor/
>```
>> REMEMBER to use ONLY verified links!

Every new VIGOR user needs to sign the constitution to be able to use the service. In the top right corner you will need to click on the **"Login"** option. A new window with **"Policies - Terms, Conditions, Privacy"** will appear.

![app2](/img/app/app_2.png)

You need to go through terms and conditions and the privacy policy linked in PDF format. The next step is to click on both options and click then **"Agree"**. You will be asked to approve the transaction on the EOS chain, and voila, now you're ready to rumble!

#### Would you like to: LEND? SAVE? BORROW?
##### Please choose from the following:

![app1](/img/app/app_1.png)

## Limits
> There is a withdrawal limit with an estimated value of **$3000 per transaction** on the platform.  
> In addition there is a withdrawal limit of **$10000** within **24h**.

## LEND
#### Lock your crypto into a lending contract and be rewarded by saving your coins or supplying them to the insurance pool.
![app3](/img/app/app_3.png)
### Lender Reward
By contributing to the Lending Pool you are rewarded VIG token as rewards. Your lender reward percentage represents the real time annualized rate that you rewarded with.
### Insurance
This shows the amount of your tokens in the Investment Pool.
### Contribution to Solvency
This shows your % share of the Insurance Pool that acts as a second line of defence for all users collateral of their loans. The first % indicator is showing the contribution to the solvency for VIGOR loans, and the second shows % for crypto loans.
### Deposit/Withdraw
You can deposit or withdraw your crypto anytime to participate in the Lending Pool. The Lending Pool is the source for supplying crypto loans (such as EOS) and also serves to “insure” all Vigor Protocol loans by absorbing bad debt.
### Value
This represents the total vaue of your tokens in the insurance pool in $.

## SAVINGS
#### Deposit VIGOR Into a Protected Savings Vault and get VIG Rewards
![app4](/img/app/app_4.png)
### Saving Reward
This shows the annualized savings rate rewarded to savers for depositing VIGOR into VIGOR Collateral. The rewards are paid to savers automatically throughout the day and denominated in VIG into their Crypto Collateral. Savings rates are floating and change continuously over time proportional to borrow rates. Savings rates **increase** **when** VIGOR market price **drops** below 1$ and **decrease when** VIGOR market price **rises** above 1$. Savings rates decrease as more savers deposit VIGOR into savings.   
### VIGOR in Savings
This shows how many VIGOR tokens you have in your savings account.
### Contribution to Pool
This shows your share of the global Savings Pool.
### Value
Value in $ of your savings in the Savings Pool.
### Deposit/Withdraw
You can deposit or withdraw your crypto anytime to participate in the Savings Pool.

## VIGOR Loans
#### Lock your coins as collateral to be able to borrow VIGOR or other crypto.
![app5](/img/app/app_5.png)
### Borrow Rate
Annualized rate that VIGOR borrowers pay in periodic premiums to insure their Crypto Collateral against downside market moves as a percentage of VIGOR Debt. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (down markets).
### Reputation
Users that maintain a VIG balance above zero (in Crypto Collateral) at all times during a borrow can be rewarded with higher **reputation** and get discounted rates. If the borrower's VIG balance (in Crypto Collateral) drops to zero the user loan will be bailed out automatically by lenders and **reputation** reset to default.
### Discount
Your good reputation reduces your borrow rate. Maximum discount is 25%.  
You can see the configuration in the contract here:
```
https://bloks.io/account/vigorlending?loadContract=true&tab=Tables&account=vigorlending&scope=vigorlending&limit=100&table=config
```
Look for **"maxdisc"** parameter in the table.
### Collateral
This shows your tokens locked in the Collateral Pool. Depositing cryptos as collateral allows you to borrow VIGOR tokens.
> During any borrow, the user collateral is at risk of bailout if the feed value of the collateral drops below the value of debt. Users must maintain a VIG balance above zero in Collateral because VIG fees are automatically deducted on a continuous basis, otherwise the user will suffer immediate bailout. A bailout is an event where the user retains excess collateral but the debt and matching amount of collateral is deducted from the borrower and taken by the lenders. There is no liquidation fee or additional charges for bailout.
>
### Value
This represents the total value of your tokens in the Collateral Pool in $.
### CR
Collateral ratio is the percentage of a loan that's secured by your locked tokens. The lower the ratio, the higher the risk of liquidation.
### Loan
When you take a loan, it will show the amount left to repay in VIGOR.
### Value
When you take a loan, it will show the amount left to repay in $.
### Borrow/Repay
Type the amount of VIGOR tokens you want to borrow or repay.

## Crypto Loans
#### Lock your coins as collateral to be able to borrow VIGOR or other crypto.
![app6](/img/app/app_6.png)
### Borrow Rate
Annualized rate that crypto borrowers pay in periodic premiums to insure their Crypto Debt against upside market moves as a percentage of VIGOR Collateral. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets).
### Reputation
Users that maintain a VIG balance above zero (in Crypto Collateral) at all times during a borrow can be rewarded with higher **reputation** and get discounted rates. If the borrower's VIG balance (in Crypto Collateral) drops to zero the user loan will be bailed out automatically by lenders and **reputation** reset to default.
### Discount
Your good reputation reduces your borrow rate. Maximum discount is 25%. You can see the configuration in the contract **[HERE](https://bloks.io/account/vigorlending?loadContract=true&tab=Tables&account=vigorlending&scope=vigorlending&limit=100&table=config)**. Look for the **"maxdisc"** parameter in the table.
### Collateral
This shows your tokens locked in the Collateral Pool. This will allow you to borrow VIGOR tokens.
> During any borrow, the user collateral is at risk of bailout if the feed value of the collateral drops below  the value of debt. Users must maintain a VIG balance above zero in Collateral because VIG fees are automatically deducted on a continuous basis, otherwise the user suffers immediate bailout. A bailout is an event where a user retains excess collateral but the debt and matching amount of collateral is deducted from the borrower and taken by the lenders. There is no liquidation fee or additional charges for bailout.
>
### Value
Value in $ of your tokens locked in the Collateral Pool.
### CR
Collateral ratio is the percentage of a loan that's secured by your locked tokens. The lower the ratio, the higher the risk of liquidation.
### Loan
When you take a loan, it will show the amount left to repay in tokens in which you took that loan.
### Value
When you take a loan, it will show the amount left to repay in $.
### Borrow/Repay
Type in the amount of tokens you want to borrow or repay.

## Recent Activity
This shows the global activity on the Vigor contract. You can see the latest data from the blockchain: exact time, EOS account name that made the transaction and a detailed description of it.
With the **"Show more"** button you can take a look at older transactions.

![app7](/img/app/app_7.png)

## Recent Bailouts
This shows recent bailouts that were executed in the vigorlending contract. It tells you the account name involved in the bailout and details like an amount liquidated and recapitalization for each account that was part of the lending pool. To find out more about the meaning of all actions in the vigorlending contract read about it **[HERE.](/docs/en/mainnet/vigor_protocol#actions)**

![app9](/img/app/app_9.png)

## Vigor Health
On Vigor Health Dashboard you can find a lot of important information about the platform. For more detailed statistics go to **[stats.vigor.ai](https://stats.vigor.ai)**

### Borrow Rates
VIGOR borrow rates reflect the supply and demand of crypto loans. With every new borrow or lend, stress tests are done on-chain in real time, and loan rates update to keep solvency in line with capital requirements. There are more incentives to have collateral based on coins with more liquidity, like EOS. VIG is a utility token and it is still needed for everyday operations, fees, etc.

![health1](/img/app/health/health_1.png)

#### In Reserve & Total Borrowed
![health2](/img/app/health/health_2.png)

+ Amount of VIG in the global reserve
+ Total amount borrowed on the Vigor platform in US dollars.


#### VIG Price & VIGOR Supply
![health3](/img/app/health/health_3.png)

+ VIG Price in dollars and in EOS
+ Total VIGOR supply, you can look at top VIGOR holders **[HERE](https://bloks.io/tokens/VIGOR-eos-vigortoken11#charts)**

### Global View
#### Lend
![health4](/img/app/health/health_4.png)
+ **Lender Reward** shows annualized rate of return on total portfolio of insurance crypto assets, insuring for downside and upside price jumps.
+ **EOS, VIG, VIGOR Insurance** shows total amount of tokens in the lending pool.
+ **Value** shows the total value of tokens in the lending pool in US dollars.

#### Savings
![health5](/img/app/health/health_5.png)
+ **Savings Rewards** - annualized savings rate rewarded to savers for depositing VIGOR low volatility payment tokens into VIGOR Collateral. The rewards are paid to savers automatically throughout the day denominated in VIG into their Crypto Collateral. Savings rates are floating and change continuously over time proportional to borrow rates. Savings rates increase when VIGOR market price drops below $1. Savings rates decrease when VIGOR market price rises above $1. Savings rates decrease as more savers deposit VIGOR into savings.
+ **VIGOR in Savings** shows total amount of VIGOR tokens in the savings pool.
+ **Value** shows the total value of tokens in the savings pool in US dollars.

#### VIGOR Loans & Crypto Loans
![health6](/img/app/health/health_6.png)
+ **Collateral** - total amount of collateral locked in the collateral pool. More about user collateral **[HERE.](./vigor_app#collateral)**
+ **Value** - Total value of all tokens in the collateral pool in US dollars.
+ **Solvency** - Solvency Ratio is the ratio of Global Funds to SCR. You can read more about it **[HERE.](./whitepaper#b-solvency)**
+ **Loan** - Amount of all VIGOR token loans taken on the platform.
+ **Value** - Value of all loans taken on the platform presented in US dollars.

![health7](/img/app/health/health_7.png)

### Final Reserve
The system stores a percentage (**"reservecut"** in config table in **[vigorlending](https://bloks.io/account/vigorlending?loadContract=true&tab=Tables&account=vigorlending&scope=vigorlending&limit=100&table=config)** contract) of VIG awarded to lenders as final reserves. VIG final reserves are used to rebalance the system if at any time the lending pool is depleted.

#### Lend
![health8](/img/app/health/health_8.png)

+ **VIG Insurance** - number of VIG tokens in the final reserve insurance pool.
+ **Value** - value of VIG tokens in US dollars in the pool.

#### Savings
![health9](/img/app/health/health_9.png)

+ **VIGOR in Savings** - number of VIGOR tokens in the final reserve savings pool.
+ **Value** - value of VIGOR tokens in US dollars in the pool.

#### Vigor Loans
![health10](/img/app/health/health_10.png)

+ **Collateral** - total amount of collateral locked in the final reserve VIGOR pool.
+ **Value (Coll.)** - shows the total value of tokens in the final reserve VIGOR pool in US dollars.
+ **Solvency** - Solvency Ratio is the ratio of Global Funds to SCR. You can read more about it **[HERE.](./whitepaper#b-solvency)**

#### Crypto Loans
![health11](/img/app/health/health_11.png)

+ **Collateral** - total amount of collateral locked in the final reserve crypto pool.
+ **Value (Coll.)** - shows the total value of tokens in the final reserve crypto pool in US dollars.
+ **Solvency** - Solvency Ratio is the ratio of Global Funds to SCR. You can read more about it **[HERE.](./whitepaper#b-solvency)**