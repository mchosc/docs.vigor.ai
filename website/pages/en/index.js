const React = require('react');
class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;
    const Top = () => (
      <div>
        <head>
            <meta
            httpEquiv="refresh"
            content="0; url=../docs/en/whitepaper.html"
            />
            <script type="text/javascript">
                window.location.href = 'docs/en/whitepaper.html';
            </script>
            <title>Vigor Documentation</title>
        </head>
        <body>
            If you are not redirected automatically, follow this <a href="../docs/en/whitepaper.html"><b>link</b></a>.
        </body>
      </div>
    );
    return (
      <div align= 'center'>
        <Top/>
      </div>
    );
  }
}
module.exports = Index;